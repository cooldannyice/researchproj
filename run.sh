#! /bin/sh

pabot --testlevelsplit --processes 2 --nostatusrc -o final.xml -d result tests
cd /builds/cooldannyice/researchproj/utilities/
mv teams.py testrail.py /builds/cooldannyice/researchproj/result/
cd /builds/cooldannyice/researchproj/result/
python teams.py $CI_JOB_ID $CI_PIPELINE_ID $BUILD_NAME
python testrail.py $TESTRAIL_USER $TESTRAIL_PASS
rm *.py
rm -r pabot_results