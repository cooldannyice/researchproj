import requests
import json
import xml.etree.ElementTree as ET
import datetime
import sys

user = sys.argv[1]
password = sys.argv[2]
testrunname = 'Membership_Smoke_Test_'+datetime.date.today().strftime('%m%d%y')

base_url = "https://petcqa.testrail.io/index.php?/api/v2/"
headers = {
  'Content-Type': 'application/json'
}


def get_cases(user,password):
  url = base_url+'get_cases/38&suite_id=2749&section_id=71581'
  response = requests.request("GET", url, headers=headers, auth=(user, password))
  resp = json.loads(response.text)
  caseids = [i['id'] for i in resp]
  return caseids

def create_run(user,password,testrunname):
    url = base_url+ 'add_run/38'
    req =   {
              "suite_id": 2749,
              "name": testrunname,
              "description":"Smoke test suite for membership web app",
              "include_all": False,
              "case_ids": get_cases(user,password)
            }

    request_body = json.dumps(req)
    response = requests.request("POST", url, headers=headers, auth=(user, password), data = request_body)
    resp = response.json()
    return str(resp['id'])


run_id = create_run(user,password,testrunname)

def update_run(testcase,result,user,password):
    url = base_url + 'add_results_for_cases/'+ run_id
    req =  {
                "results": [
                    {
                        "case_id": testcase,
                        "status_id": result
                    }
                ]
            }
    request_body = json.dumps(req)
    requests.request("POST", url, headers=headers, auth=(user, password), data = request_body)
    

def extract_testcase_status_xml_update(user,password):
    tree = ET.parse('final.xml')
    root = tree.getroot()
    for type_tag in root.findall('./statistics/tag/stat'):
      testcase = type_tag.text 
      result = 5 if int(type_tag.attrib['pass']) == 0 else 1 
      update_run(testcase,result,user,password)

extract_testcase_status_xml_update(user,password)

