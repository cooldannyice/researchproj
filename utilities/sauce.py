import requests
import json

base_url = "https://saucelabs.com/rest/v1/petcosaucelabs/jobs/"

def update_test_status(job_id,value,user,password):
    url = base_url+job_id
    headers = {
      'Content-Type': 'application/json'
    }
    payload_base = {"passed":value}
    payload = json.dumps(payload_base)
    response = requests.request("PUT", url, headers=headers, auth=(user, password), data = payload)

