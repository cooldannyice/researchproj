import requests
import json
import datetime
import sys
import xml.etree.ElementTree as ET

jobid = sys.argv[1]
pipelineid = sys.argv[2]
project = sys.argv[3]

tree = ET.parse('final.xml')
root = tree.getroot()
pass_count =  int(root[1][0][1].attrib['pass'])
fail_count =  int(root[1][0][1].attrib['fail'])

subtitle = datetime.date.today().strftime('%m/%d/%y')
url = "https://outlook.office.com/webhook/41b750b6-12ef-48ad-8e18-973298ff771d@12810367-27b2-41b1-a589-7c74628d957d/IncomingWebhook/2a15637f1f4f41a88c446f469cacd852/750cbfb8-5ae1-4633-9b32-ad23726f1f4f"
robot_report_base = 'https://gitlab.com/petco/PetCoachTribe/petco-membership-client-tests/-/jobs/'
pipeline_base = 'https://gitlab.com/petco/PetCoachTribe/petco-membership-client-tests/-/pipelines/'

def send_status_teams(jobid,pipelineid,project,pass_count,fail_count):
    robot_report_link = robot_report_base+str(jobid)+'/artifacts/download'
    pipeline_link = pipeline_base+str(pipelineid)

    payload = {
    "@type": "MessageCard",
    "@context": "https://schema.org/extensions",
    "summary": "Membership Automated Test Results",
    "themeColor": "0078D7",
    "sections": [
        {
            "activityTitle": "{} UI E2E Automation Status :".format(project),
            "activitySubtitle": "**ENV: QA**" ,
            "activityText": "**DATE: {}**".format(subtitle),
            "facts": [
                {
                    "name": "Summary:",
                    "value": "Passed:{} Failed:{}".format(pass_count,fail_count)
                }
            ],
            "potentialAction": [
                {
                    "@type": "OpenUri",
                    "name": "Robot Report",
                    "targets": [
                        {
                            "os": "default",
                            "uri": robot_report_link
                        }
                    ]
                },
                {
                    "@type": "OpenUri",
                    "name": "Gitlab CI",
                    "targets": [
                        {
                            "os": "default",
                            "uri": pipeline_link
                        }
                    ]
                }
            ]
        }
    ]
}

    data = json.dumps(payload)
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data = data)

send_status_teams(jobid,pipelineid,project,pass_count,fail_count)

    
    
