import requests
import json
import xml.etree.ElementTree as ET

def send_results_to_channel(total_count,pass_count,fail_count):
    url = "https://hooks.slack.com/services/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"
    headers = {
    'Content-Type': 'application/json'
    }
    payload  = {
        "text": "Membership Smoketest Status",
        "attachments": [
            {
                "fields": [
                    {
                        "title": "Total : {} ".format(total_count)
                    },
                    {
                        "title": "Pass  : {} ".format(pass_count)
                    },
                    {
                        "title": "Fail  : {} ".format(fail_count)
                    }
                ]
            }
        ]
    }
    data = json.dumps(payload)
    response = requests.request("POST", url, headers=headers, data = data)

def extract_results_from_xml_send_slack():
    tree = ET.parse('final.xml')
    root = tree.getroot()
    pass_count =  int(root[1][0][1].attrib['pass'])
    fail_count =  int(root[1][0][1].attrib['fail'])
    total_count = pass_count + fail_count
    send_results_to_channel(total_count,pass_count,fail_count)

extract_results_from_xml_send_slack()
