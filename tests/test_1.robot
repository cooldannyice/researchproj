*** Settings ***
Library         SeleniumLibrary
Library         BuiltIn
Test Teardown   Close browser


*** Variables ***
${URL}                  https://int1-www.petco.com/shop/PetcoMembershipsView?langId=-1&langId=-1&catalogId=10051&storeId=10151&redirectBack=true
${browsername}          chrome
${platform}             Windows 10
${version}              latest
${SAUCE_USERNAME}       ss
${SAUCE_ACCESS_KEY}     sss
${tunnelidentifier}     PetcoVM
${capabilities}         browserName:${browserName},platform:${platform},version:${version},username:${SAUCE_USERNAME},accessKey:${SAUCE_ACCESS_KEY},name:${SUITE_NAME},tunnelidentifier:${tunnelidentifier}
${remote_url}           http://ondemand.saucelabs.com/wd/hub


*** Test cases ***
T1
    [Tags]  652755
    Open browser    ${URL}  headlesschrome  #remote_url=${remote_url}    desired_capabilities=${capabilities}
    wait until page contains element            //input[@name="logonId"] 

T2
    [Tags]  653105
    Open browser    ${URL}  headlesschrome  #remote_url=${remote_url}    desired_capabilities=${capabilities}
    wait until page contains element            //input[@name="logonId"]

T3
    [Tags]  662651
    Open browser    ${URL}  headlesschrome  #remote_url=${remote_url}    desired_capabilities=${capabilities}
    wait until page contains element            //input[@name="logonId"]

T4
    [Tags]  653108
    Open browser    ${URL}  headlesschrome  #remote_url=${remote_url}    desired_capabilities=${capabilities}
    wait until pag contains element            //input[@name="logonId"]